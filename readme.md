# SteamVR Layout Bug Demo

A demonstration of a bug in SteamVR where SteamVR violates the OpenXR specification when used with a single swapchain for both eyes and two array layers per swapchain image to store the two eye views.

## Usage

1. Build with `./build.sh`.
2. Run with `./demo dual`. The program will run and display a solid white color inside your headset and emit no recurring validation errors.
3. Close the program and re-run with `./demo single`. The program will run and again display a solid white color inside your headset, but now it will emit continuous Vulkan validation errors in the terminal.

## Explanation

When run with the `dual` flag the program initializes two entirely separate `XrSwapchain` objects, one for each eye, and a set of swapchain images for each. It fills these with the white color and then submits them back to SteamVR in `VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL`. When run with the `single` flag the program initializes a single `XrSwapchain` object and one set of swapchain images, each with two array layers: one for each eye. It fills these with the white color using Vulkan's multiview functionality and then submits them back to SteamVR in `VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL` as before. Unlike when there were two swapchains however a validation error occurs on every frame.

The validation error encountered is this:

```
vkQueueSubmit(): pSubmits[0].pCommandBuffers[0] command buffer VkCommandBuffer expects VkImage (subresource: aspectMask 0x1 array layer 1, mip level 0) to be in layout VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL--instead, current layout is VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL.
```

This originates from a `vkQueueSubmit` call somewhere inside SteamVR, not in the application code. This indicates that SteamVR is expecting the second layer (`arrayLayerIndex = 1`) to be in layout `VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL`. This violates the OpenXR standard which states in section 12.20 on the `XR_KHR_vulkan_enable` extension:

> When an application releases a swapchain image by calling `xrReleaseSwapchainImage`, in a session created using `XrGraphicsBindingVulkanKHR`, the OpenXR runtime must interpret the image as:
> 
> • Having a memory layout compatible with `VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL` for color images, or `VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL` for depth images.
